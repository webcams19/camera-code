##
# config.py - Configuration tools 'n stuff
#
# Written by Nicholas Hollander <nhhollander@wpi.edu>
# Written on 2019-05-03
#

import json
import os
import errno

# Configuration dictionary
config = dict()

##
# Read the configuration.
# This function reads the configuration file from disk and saves the variables
# to the configuration map.
def read():
    global config
    try:
        # Open the file
        cfile = open('config.json','r')
        # Parse the file
        config = json.load(cfile)
        # Close the file
        cfile.close()
        return True
    except IOError as e:
        # Something went wrong while reading the file
        print("\033[1;31mERROR:\033[0m "
        "Something went wrong while reading \033[1mconfig.json\033[0m: {}({}):{}"
        .format(errno.errorcode[e.errno], e.errno, os.strerror(e.errno)))
        return False
