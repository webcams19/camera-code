##
# ec25.py - Helper functions for the Quectel EC25-A module
#
# Written by Nicholas Hollander <nhhollander@wpi.edu>
# Written on 2019-05-16
#

import serial
import time
import threading
from ast import literal_eval
import traceback

# Global connection instance
cmd_connection = None
# GPS feedback connection
gps_connection = None

# Run flag - set to False to stop threads
run = True

######################
## STATUS VARIABLES ##
######################

# Sim card insertion status
sim_card_inserted = None
# Sim card auto-update enabled
sim_card_inserted_auto = None
# Scanned carriers
available_carriers = None
# GPS data
gps_data = None
# Functionality
functionality = 1

# Handlers
handlers = dict()

#############################################
## MAGICAL SETUP AND CONFIGURATION METHODS ##
#############################################

# Connect
def init(enable_gps=False):
    global cmd_connection
    global gps_connection

    # Connect
    print("Connecting to ec25 control interface")
    cmd_connection = serial.Serial("/dev/ttyUSB2", timeout=1)

    # Configure the error mode (verbose errors)
    cmd_connection.write("AT+CMEE=2\r\n".encode())

    # Start the input thread
    primary_thread = threading.Thread(target=__primary_thread,name='PrimaryThread')
    primary_thread.start()

    # Check for GPS
    if enable_gps:
        print("Connecting to gps feedback interface")
        # Enable GPS
        do_enable_gps()
        # Open the GPS connection
        gps_connection = serial.Serial("/dev/ttyUSB1", timeout=4)
        # Start the gps input thread
        gps_thread = threading.Thread(target=__gps_thread,name='GPSThread')
        gps_thread.start()

    # Done
    print("Connection complete")

# Enable GPS
def do_enable_gps():
    # Disable GPS first (leftover session bug)
    cmd_connection.write("AT+QGPSEND\r\n".encode())
    time.sleep(1)
    # Send the gps enable command
    cmd_connection.write("AT+QGPS=1\r\n".encode())

#########################
## THREADS AND PARSING ##
#########################

# Primary query response thread
def __primary_thread():
    while run:
        # Read input - The timeout specified when the serial port was opened
        # means that this method will return a blank message after one second
        # of waiting if no new messages are available
        line = cmd_connection.readline().decode()
        if line == "":
            continue
        # Ignore non-update messages, which always begin with a plus.
        if not line[0] == "+":
            continue
        # TODO: Parse command here

# GPS data thread
def __gps_thread():

    # GPS coordinate decode method
    def gps_decode(coord):
        x = coord.split(".")
        head = x[0]
        tail = x[1]
        deg = head[0:-2]
        minute = head[-2:]
        return int(deg) + (float(minute) * (1/60))

    global gps_data
    while run:
        # Get the next line of GPS data
        line = gps_connection.readline().decode()
        print(line)
        # TODO: Add parsing for other gps data types, incl. altitude
        # GPRMC -> Recommended minimum specific GPS/Transit data
        if line.startswith("$GPRMC"):
            # Split the data 
            sdata = line.split(",")
            if sdata[2] == "V":
                # Satellite data is not available
                gps_data = None
                continue
            # Parse the data
            time = sdata[1][0:2] + ":" + sdata[1][2:4] + ":" + sdata[1][4:6]
            lat = gps_decode(sdata[3])  #latitude
            dirLat = sdata[4]           #latitude direction N/S
            lon = gps_decode(sdata[5])  #longitute
            dirLon = sdata[6]           #longitude direction E/W
            speed = sdata[7]            #Speed in knots
            trCourse = sdata[8]         #True course
            date = sdata[9][0:2] + "/" + sdata[9][2:4] + "/" + sdata[9][4:6] # date
            variation = sdata[10]       #variation
            dc = sdata[11].split("*")   # degree checksum
            degree = dc[0]              #degree
            checksum = dc[1]            #checksum
            # Combine to form the gps data
            gps_data = (time,lat,dirLat,lon,dirLon,speed,trCourse,date,variation,degree,checksum)

###########################
## FUNCTIONS AND QUERIES ##
###########################

##
# Sets the device functionality level
# 0 - Minimum Functionality
# 1 - Full Functionality
# 4 - Disable RX and TX
def set_functionality(level):
    global functionality
    # Verify
    if not (level == 0 or level == 1 or level == 4):
        print("Invalid functionality setting [{}]".format(level))
    else:
        cmd_connection.write("AT+CFUN={}\r\n".format(level).encode())
        # Hacky fix - change this to use something nicer later.  Or never.
        functionality = level
