#!/usr/bin/python3

import config
import ec25
import os
import time


os.chdir("/home/pi/code")

config.read()

def sim_card(auto,inserted):
    if inserted:
        print("\033[1;32mThe sim card is present\033[0m")
    else:
        print("\033[1;31mNo sim card detected!\033[0m")

ec25.handlers_qsimstat.append(sim_card)

def carriers(c):
    counter = 1
    for carrier in c:
        print("\033[1;31m{}:\033[0m".format(counter))
        print("  \033[1;31mStatus:\033[0m {}".format(carrier[0]))
        print("  \033[1;31mName:\033[0m {}".format(carrier[1]))
        print("  \033[1;31mName (short):\033[0m {}".format(carrier[2]))
        print("  \033[1;31mID:\033[0m {}".format(carrier[3]))
        print("  \033[1;31mTech:\033[0m {}".format(carrier[4]))
        counter += 1

ec25.handlers_cops.append(carriers)

ec25.connect()

ec25.query_available_networks()