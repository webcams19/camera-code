#!/usr/bin/python3 -u

##
# camera.py - Extreme Webcam Controller 9000
#
# Written by Nicholas Hollander <nhhollander@wpi.edu>
# Written on 2019-05-03
#

from picamera import PiCamera
import datetime
import os
import config
import math
import time
import upload
import subprocess
import ec25

# Print something to separate the logs
datestr = datetime.datetime.now().strftime("%T on %F")
msg = "STARTING UP AT {}".format(datestr)
print("\n  \033[1;33m\u2554{}\u2557".format("\u2550"*len(msg)))
print("  \u2551{}\u2551".format(msg))
print("  \u255A{}\u255D\033[0m\n".format("\u2550"*len(msg)))

#########
# SETUP #
#########

# Print the PID
print("My PID is \033[1;33m{}\033[0m".format(os.getpid()))
print("Kill with \033[1m~/scripts/kill_children.sh {}\033[0m".format(os.getpid()))
# Change the working directory
print("\033[1mChanging working directory...\033[0m")
os.chdir("/home/pi")
print("Working directory is \033[1m{}\033[0m".format(os.getcwd()))
print("\033[1mConnecting to EC25...\033[0m")
ec25.init()

# Read the configuration
print("\033[1mLoading Configuration...\033[0m")
if not config.read():
    print("\033[31mNo configuration loaded - terminating\033[0m")
    exit()

# Initialize EC25
print("\033[1mConnecting to EC25\033[0m")
ec25.init(config.config['ec25']['enable_gps'])

# Set up and connect to the camera
print("\033[1mSetting up camera...\033[0m")
camera = PiCamera()
camera.resolution = (
    config.config['camera']['resolution']['x'],
    config.config['camera']['resolution']['y'])
print("Using resolution to \033[1m{}\033[0m".format(camera.resolution))

# Get the interval
print("\033[1mConfiguring interval...\033[0m")
start_time = config.config['interval']['start_time']
stop_time  = config.config['interval']['stop_time']
start_hour = math.floor(start_time / 60)
start_min  = start_time % 60
stop_hour  = math.floor(stop_time / 60)
stop_min   = stop_time % 60
interval   = config.config['interval']['interval']
print("Taking pictures every \033[1;33m{}\033[0m minutes, starting at \033[1;33m{:02d}:{:02d}\033[0m and ending at \033[1;33m{:02d}:{:02d}\033[0m!".format(interval, start_hour, start_min, stop_hour, stop_min))

#############
# MAIN LOOP #
#############

while True:
    # Get the current time
    ctime = datetime.datetime.now()
    # Extract components of the time
    year =   ctime.year
    month =  ctime.month
    day =    ctime.day
    hour =   ctime.hour
    minute = ctime.minute
    second = ctime.second
    ttime =  (hour * 60) + minute
    # Compare time to start of day and end of day bounds
    if ttime < start_time:
        # It's too early for this!
        time.sleep((start_time - ttime) * 60)
        continue
    elif ttime > stop_time:
        # It's too late for this!
        print("\033[35mIt's too late to be taking pictures!\033[0m")
        if config.config['power']['disconnect_at_night']:
            print("Disabling EC25 (Goodnight)")
            ec25.set_functionality(0)
        # Wait until midnight
        time.sleep(((24 * 60) - ttime) * 60)
        continue
    # Enable EC25 only if it is not already enabled
    if not ec25.functionality == 1:
        print("Enabling EC25")
        ec25.set_functionality(1)
    # Generate the timestamped filename
    fname = "data/{}-{:04d}-{:02d}-{:02d}_{:02d}-{:02d}-{:02d}.jpg".format(config.config['capture']['name_base'],year,month,day,hour,minute,second)
    print("Saving image to \033[1m{}\033[0m".format(fname))
    camera.capture(fname)
    # Compress and annotate the image
    subprocess.run(['/home/pi/scripts/annotate.sh',fname,'{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}'.format(year,month,day,hour,minute,second),config.config['capture']['name_full']])
    # Upload the image and delete it (on successful upload)
    upload.upload(fname, True)
    print("Waiting \033[1m{}\033[0m seconds before taking next picture".format(interval * 60))
    time.sleep(interval * 60)
