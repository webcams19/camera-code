##
# upload.py - Tool for uploading images to the web server
#
# Written by Nicholas Hollander <nhhollander@wpi.edu>
# Written on 2019-05-16

import pysftp
import threading
import config
import traceback
import time
import os
import ec25

# List of threads.
threads = []

##
# Upload a file.
# This function attempts to upload a file to the server, retrying on failure
# until the retry limit is reached.  If the file is uploaded, it is deleted
# from the device.  If the upload fails completely, then the file will be
# left on the device.
def upload(fname, delete=False):
    # Start the upload thread
    t = threading.Thread(target=__upload, args=(fname,delete))
    threads.append(t)
    t.start()

##
# Upload Thread.
# Performs the nitty gritty of the above function without blocking program
# execution.
def __upload(fname, delete):
    # Load configuration information
    username = config.config['server']['username']
    keyfile = config.config['server']['key_file']
    hostname = config.config['server']['hostname']
    directory = config.config['server']['directory']
    retry_count = config.config['server']['retry_count']
    retry_delay = config.config['server']['retry_delay']
    powersave_enabled = config.config['power']['disconnect_after_upload']
    # Upload loop
    for i in range(0, retry_count):
        # Print some information
        print("Uploading file \033[1m{}\033[0m to \033[1m{}@{}:{}\033[0m (Attempt \033[33m{}\033[0m of \033[33m{}\033[0m)".format(fname, username, hostname, directory, i + 1, retry_count))
        # Catch connection errors
        try:
            # Connect and upload the file
            connection = pysftp.Connection(hostname, username=username, private_key=keyfile)
            connection.chdir(directory)
            connection.put(fname, preserve_mtime=True)
            # Delete file from local device
            if delete:
                os.remove(fname)
            print("Upload of \033[1m{}\033[0m complete!".format(fname))
            if powersave_enabled:
                print("Disabling EC25 (Upload complete)")
                ec25.set_functionality(0)
            return
        except Exception as e:
            # Print the stack trace
            print("\033[1;31mError:\033[0m Something went wrong while uploading \033[1m{}\033[0m: (\033[1;31m{}\033[0m)".format(fname, type(e).__name__))
            print("\033[1;31m=== Begin traceback ===\033[0m")
            traceback.print_tb(e.__traceback__)
            print("\033[1;31m=== End traceback ===\033[0m")
            # Delay
            delay_time = retry_delay * (i + 1)
            print("Retrying upload in \033[1m{}\033[0m seconds.".format(delay_time))
            time.sleep(delay_time)

